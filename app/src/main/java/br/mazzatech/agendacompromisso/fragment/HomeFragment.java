package br.mazzatech.agendacompromisso.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.tibolte.agendacalendarview.AgendaCalendarView;
import com.github.tibolte.agendacalendarview.CalendarPickerController;
import com.github.tibolte.agendacalendarview.models.BaseCalendarEvent;
import com.github.tibolte.agendacalendarview.models.CalendarEvent;
import com.github.tibolte.agendacalendarview.models.DayItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import br.mazzatech.agendacompromisso.R;
import br.mazzatech.agendacompromisso.activity.HomeActivity;
import br.mazzatech.agendacompromisso.activity.CadastroCompromissoActivity;
import br.mazzatech.agendacompromisso.model.CompromissoBean;
import br.mazzatech.agendacompromisso.task.PersistirCompromissoDelegate;
import br.mazzatech.agendacompromisso.task.PersistirCompromissoTask;
import br.mazzatech.agendacompromisso.util.Constantes;
import br.mazzatech.agendacompromisso.util.DrawableEventRenderer;

/**
 * Created by 809549 on 06/06/2016.
 */
public class HomeFragment extends Fragment implements CalendarPickerController, View.OnClickListener, PersistirCompromissoDelegate {
    private AgendaCalendarView agendaCalendarView;
    private FloatingActionButton floatButton;
    private boolean atulizarCalendario = false;
    private List<CompromissoBean> compromissoBeanList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        agendaCalendarView = (AgendaCalendarView) view.findViewById(R.id.agenda_calendar_view);
        agendaCalendarView.setOnClickListener(this);
        // minimum and maximum date of our calendar
        // 2 month behind, one year ahead, example: March 2015 <-> May 2015 <-> May 2016
        Calendar minDate = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();

        minDate.add(Calendar.MONTH, -2);
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        maxDate.add(Calendar.YEAR, 1);


        List<CalendarEvent> eventList = new ArrayList<>();

        agendaCalendarView.init(eventList, minDate, maxDate, Locale.getDefault(), this);
        agendaCalendarView.addEventRenderer(new DrawableEventRenderer());

        floatButton = (FloatingActionButton) view.findViewById(R.id.fab);
        floatButton.setOnClickListener(this);

        CompromissoBean compromissoBean = new CompromissoBean();
        compromissoBean.setCodfuncao(Constantes.COD_FUNCAO_PERSISTIR_COMPROMISSO);
        compromissoBean.setIdUser(((HomeActivity) getActivity()).IdUsario);
        List<CompromissoBean> list = new ArrayList<>();
        list.add(compromissoBean);
        new PersistirCompromissoTask(getActivity(), this).execute(list);

        return view;
    }

    @Override
    public void onDaySelected(DayItem dayItem) {

    }

    @Override
    public void onEventSelected(CalendarEvent event) {
        int id = (int) event.getId();
        CompromissoBean compromissoBean = compromissoBeanList.get(id);
        Intent intent = new Intent(getActivity(), CadastroCompromissoActivity.class);
        intent.putExtra("idUsuario", ((HomeActivity) getActivity()).IdUsario);
        intent.putExtra("CompromissoBean", compromissoBean);
        startActivity(intent);
        atulizarCalendario = true;
    }

    @Override
    public void onScrollToDate(Calendar calendar) {

    }

    @Override
    public void onClick(View v) {
        System.out.println("ID = > " + v.getId());
        switch (v.getId()){
            case R.id.fab:
                Intent intent = new Intent(getContext(), CadastroCompromissoActivity.class);
                intent.putExtra("idUsuario", ((HomeActivity) getActivity()).IdUsario);
                startActivity(intent);
                atulizarCalendario = true;


        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (atulizarCalendario){
            CompromissoBean compromissoBean = new CompromissoBean();
            compromissoBean.setCodfuncao(Constantes.COD_FUNCAO_PERSISTIR_COMPROMISSO);
            compromissoBean.setIdUser(((HomeActivity) getActivity()).IdUsario);
            List<CompromissoBean> list = new ArrayList<>();
            list.add(compromissoBean);
            new PersistirCompromissoTask(getActivity(), this).execute(list);
            atulizarCalendario = false;
        }
    }


    @Override
    public void onTaskCompleteCompromissoDelegate(List<CompromissoBean> resultado) {
        if(resultado.get(0).getIdCompromisso() != -1){
            compromissoBeanList = resultado;
            // minimum and maximum date of our calendar
            // 2 month behind, one year ahead, example: March 2015 <-> May 2015 <-> May 2016
            Calendar minDate = Calendar.getInstance();
            Calendar maxDate = Calendar.getInstance();

            minDate.add(Calendar.MONTH, -2);
            minDate.set(Calendar.DAY_OF_MONTH, 1);
            maxDate.add(Calendar.YEAR, 1);

            List<CalendarEvent> eventList = new ArrayList<>();
            mockList(eventList, resultado);
            agendaCalendarView.init(eventList, minDate, maxDate, Locale.getDefault(), this);
            agendaCalendarView.addEventRenderer(new DrawableEventRenderer());

        }else {
            ((HomeActivity) getActivity()).alertError("Não há compromisso(s), cadastrado.").show();
        }
    }

    private void mockList(List<CalendarEvent> eventList ,List<CompromissoBean> compromissoBeen) {
        if(compromissoBeen.size() > 0){
            for (int x = 0; x < compromissoBeen.size(); x++){

                StringBuilder stringHoraLocal = new StringBuilder();
                stringHoraLocal.append(compromissoBeen.get(x).getLocal());
                stringHoraLocal.append(" ");

                if(compromissoBeen.get(x).getHora().getHours() < 10){
                    stringHoraLocal.append("0");
                    stringHoraLocal.append(compromissoBeen.get(x).getHora().getHours());
                }else {
                    stringHoraLocal.append(compromissoBeen.get(x).getHora().getHours());
                }
                stringHoraLocal.append(":");
                if(compromissoBeen.get(x).getHora().getMinutes() < 10){
                    stringHoraLocal.append("0");
                    stringHoraLocal.append(compromissoBeen.get(x).getHora().getMinutes());
                }else {
                    stringHoraLocal.append(compromissoBeen.get(x).getHora().getMinutes());
                }
                stringHoraLocal.append("hs");

                String[] dataStringInicio = compromissoBeen.get(x).getDtdInicio().split("-");
                Calendar startTime1 = Calendar.getInstance();
                startTime1.set(Integer.parseInt(dataStringInicio[0]), Integer.parseInt(dataStringInicio[1]),  Integer.parseInt(dataStringInicio[2]));

                String[] dataStringFim = compromissoBeen.get(x).getDtdFim().split("-");
                Calendar endTime1 = Calendar.getInstance();
                endTime1.set(Integer.parseInt(dataStringFim[0]), Integer.parseInt(dataStringFim[1]),  Integer.parseInt(dataStringFim[2]));
                BaseCalendarEvent event = new BaseCalendarEvent(compromissoBeen.get(x).getAssunto(), compromissoBeen.get(x).getDescricao(), stringHoraLocal.toString(), ContextCompat.getColor(getActivity(), R.color.orange_dark), startTime1, endTime1, false);
                event.setId(x);
                eventList.add(event);
            }
        }
    }
}
