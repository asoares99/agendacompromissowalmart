package br.mazzatech.agendacompromisso.fragment;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import br.mazzatech.agendacompromisso.R;
import br.mazzatech.agendacompromisso.activity.HomeActivity;
import br.mazzatech.agendacompromisso.dao.AgendaDAO;
import br.mazzatech.agendacompromisso.model.UserBean;
import br.mazzatech.agendacompromisso.notification.BroadcastManager;
import br.mazzatech.agendacompromisso.task.UserLoginDelegate;
import br.mazzatech.agendacompromisso.task.UserLoginTask;
import br.mazzatech.agendacompromisso.util.Constantes;
import br.mazzatech.agendacompromisso.util.RemiderTime;

/**
 * Created by 809549 on 08/06/2016.
 */
public class LoginFragment extends Fragment implements View.OnClickListener, UserLoginDelegate {
    private EditText edtEmail;
    private EditText edtSenha;
    private CheckBox ckbManterLogado;
    private UserBean userBean = new UserBean();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        edtEmail = (EditText) view.findViewById(R.id.edtEmail);
        edtSenha = (EditText) view.findViewById(R.id.edtSenha);
        ckbManterLogado = (CheckBox) view.findViewById(R.id.ckbManterLogado);

        Button btnEntrar = (Button) view.findViewById(R.id.btnEntrar);
        btnEntrar.setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(View v) {
        hideKeyboard(v);
        switch (v.getId()) {
            case R.id.btnEntrar:
                if (edtEmail.getText().toString().trim().equalsIgnoreCase("")) {
                    edtEmail.setError("Por favor, informe o email.");
                } else if (edtSenha.getText().toString().trim().equalsIgnoreCase("")) {
                    edtSenha.setError("Por favor, informe sua senha.");
                } else {
                    userBean.setEmail(edtEmail.getText().toString());
                    userBean.setPass(edtSenha.getText().toString());
                    userBean.setManterLogado(ckbManterLogado.isChecked());
                    userBean.setCodfuncao(Constantes.COD_FUNCAO_LOGIN);
                    new UserLoginTask(getActivity(), this).execute(userBean);
                }
                break;
        }
    }

    public void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm.isActive()) {
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            }

        }
    }

    @Override
    public void onTaskCompleteLoginDelegate(UserBean resultado) {

        if (resultado != null && resultado.getStatusCode() != HttpURLConnection.HTTP_OK) {
            ((HomeActivity) getActivity()).alertError(resultado.getMensagem()).show();
        } else {
            if (resultado.getIdUsuario() == 0) {
                ((HomeActivity) getActivity()).alertError(resultado.getMensagem()).show();
            } else {
                if (ckbManterLogado.isChecked()) {
                    AgendaDAO agendaDAO = new AgendaDAO(getActivity());
                    agendaDAO.insertUser(userBean);
                    agendaDAO.closeDB();
                }
                Fragment fragment = new HomeFragment();
                FragmentManager frgManager = getActivity().getSupportFragmentManager();
                frgManager.beginTransaction().replace(R.id.frame, fragment).commit();
                ((HomeActivity) getActivity()).getEmailUser(resultado.getIdUsuario(), resultado.getEmail());

            }
        }
    }

}
