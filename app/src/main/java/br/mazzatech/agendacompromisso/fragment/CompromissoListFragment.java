package br.mazzatech.agendacompromisso.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import br.mazzatech.agendacompromisso.R;
import br.mazzatech.agendacompromisso.activity.CadastroCompromissoActivity;
import br.mazzatech.agendacompromisso.activity.HomeActivity;
import br.mazzatech.agendacompromisso.adapter.CompromissoListAdapter;
import br.mazzatech.agendacompromisso.dao.AgendaDAO;
import br.mazzatech.agendacompromisso.model.CompromissoBean;
import br.mazzatech.agendacompromisso.model.UserBean;
import br.mazzatech.agendacompromisso.task.UserLoginDelegate;
import br.mazzatech.agendacompromisso.task.UserLoginTask;
import br.mazzatech.agendacompromisso.util.Constantes;

/**
 * Created by 809549 on 08/06/2016.
 */
public class CompromissoListFragment extends Fragment implements AdapterView.OnItemClickListener {
    private ListView listViewCompromisso;
    private CompromissoListAdapter adapter;
    private List<CompromissoBean> listCompromisso = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.conteudo_compromisso, container, false);

        listViewCompromisso = (ListView) view.findViewById(R.id.lvCompromisso);
        listViewCompromisso.setOnItemClickListener(this);

        listCompromisso = ((HomeActivity)getActivity()).getListCompromisso();
        adapter = new CompromissoListAdapter(getActivity(), listCompromisso);
        listViewCompromisso.setAdapter(adapter);


        return view;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(getActivity(), CadastroCompromissoActivity.class);
        intent.putExtra("idUsuario", ((HomeActivity) getActivity()).IdUsario);
        intent.putExtra("CompromissoBean", listCompromisso.get(position));
        startActivity(intent);
    }
}
