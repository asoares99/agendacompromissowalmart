package br.mazzatech.agendacompromisso.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.net.HttpURLConnection;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Timer;

import br.mazzatech.agendacompromisso.R;
import br.mazzatech.agendacompromisso.dao.AgendaDAO;
import br.mazzatech.agendacompromisso.model.CompromissoBean;
import br.mazzatech.agendacompromisso.task.AlarmeTask;
import br.mazzatech.agendacompromisso.task.CompromissoDelegate;
import br.mazzatech.agendacompromisso.task.CompromissoTask;
import br.mazzatech.agendacompromisso.util.Constantes;

public class CadastroCompromissoActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener, CompromissoDelegate {

    private Toolbar toolbar;
    private ImageView imgNew;
    private ImageView imgDelete;
    private EditText edtAssunto;
    private EditText edtDescricao;
    private EditText edtLocal;
    private TextView edtHora;
    private CheckBox ckbDiaInteiro;
    private TextView edtDataInicio;
    private TextView edtDataFim;
    private DatePickerDialog datePickerDialog;
    private Date dataInicio = new Date();
    private Date dataFim = new Date();
    private Calendar calendar = Calendar.getInstance();
    private AgendaDAO agendaDAO;
    private List<CompromissoBean> listCompromisso = new ArrayList<>();
    private CompromissoBean compromissoBean;
    private Time timeHora;
    private long idUsuario;
    private boolean edicaoCompromisso = false;
    private int idCompromisso ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_comp);

        toolbar = (Toolbar) findViewById(R.id.toolbarNew);
        imgNew = (ImageView) toolbar.findViewById(R.id.imgNew);
        imgDelete = (ImageView) toolbar.findViewById(R.id.imgDelete);
        imgNew.setOnClickListener(this);
        imgDelete.setOnClickListener(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Novo Compromisso");

        agendaDAO = new AgendaDAO(this);

        edtAssunto = (EditText) findViewById(R.id.edtAssunto);
        edtDescricao = (EditText) findViewById(R.id.edtDescricao);
        edtLocal = (EditText) findViewById(R.id.edtLocal);

        edtHora = (TextView) findViewById(R.id.edtHora);
        edtHora.setOnTouchListener(this);

        ckbDiaInteiro = (CheckBox) findViewById(R.id.ckbDiaInteiro);
        ckbDiaInteiro.setOnClickListener(this);

        edtDataInicio = (TextView) findViewById(R.id.edtDataInicio);
        edtDataInicio.setOnTouchListener(this);

        edtDataFim = (TextView) findViewById(R.id.edtDataFim);
        edtDataFim.setOnTouchListener(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            idUsuario = bundle.getLong("idUsuario");

            if (bundle.containsKey("CompromissoBean")) {
                compromissoBean = (CompromissoBean) bundle.getSerializable("CompromissoBean");
                edtAssunto.setText(compromissoBean.getAssunto());
                edtDescricao.setText(compromissoBean.getDescricao());
                edtLocal.setText(compromissoBean.getLocal());

                StringBuilder stringHora = new StringBuilder();
                timeHora = compromissoBean.getHora();
                if (compromissoBean.getHora() != null && !compromissoBean.isDiaInteiro()) {

                    if (compromissoBean.getHora().getHours() < 10) {
                        stringHora.append("0");
                        stringHora.append(compromissoBean.getHora().getHours());
                    } else {
                        stringHora.append(compromissoBean.getHora().getHours());
                    }
                    stringHora.append(":");
                    if (compromissoBean.getHora().getMinutes() < 10) {
                        stringHora.append("0");
                        stringHora.append(compromissoBean.getHora().getMinutes());
                    } else {
                        stringHora.append(compromissoBean.getHora().getMinutes());
                    }
                    edtHora.setText(stringHora.toString());
                }else{
                    edtHora.setEnabled(false);
                }
                ckbDiaInteiro.setChecked(compromissoBean.isDiaInteiro());

                String[] SplitDataInicio = compromissoBean.getDtdInicio().split("-");
                edtDataInicio.setText(SplitDataInicio[2] + "/" + SplitDataInicio[1] + "/" + SplitDataInicio[0]);

                String[] SplitdataFim = compromissoBean.getDtdFim().split("-");
                edtDataFim.setText(SplitdataFim[2] + "/" + SplitdataFim[1] + "/" + SplitdataFim[0]);
                edtDataFim.setEnabled(true);
                edtDataFim.setAlpha(1);


                dataInicio.setDate(Integer.parseInt(SplitDataInicio[2]));
                dataInicio.setYear(Integer.parseInt(SplitDataInicio[0]));
                dataInicio.setMonth(Integer.parseInt(SplitDataInicio[1]));

                dataFim.setDate(Integer.parseInt(SplitdataFim[2]));
                dataFim.setYear(Integer.parseInt(SplitdataFim[0]));
                dataFim.setMonth(Integer.parseInt(SplitdataFim[1]));
                edicaoCompromisso = true;
                idCompromisso = compromissoBean.getIdCompromisso();
                imgDelete.setVisibility(View.VISIBLE);

            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ckbDiaInteiro:
                if (ckbDiaInteiro.isChecked()) {
                    edtHora.setEnabled(false);
                    edtHora.setTextSize(18);
                    edtHora.setTextColor(getResources().getColor(android.R.color.darker_gray));
                } else {
                    edtHora.setTextSize(22);
                    edtHora.setEnabled(true);
                    edtHora.setTextColor(getResources().getColor(android.R.color.black));
                }
                break;

            case R.id.imgNew:
                montarList();
                break;

            case R.id.imgDelete:
                deleteCompromisso();
                break;

        }

    }

    private void montarList() {
        if (edtAssunto.getText().toString().trim().equalsIgnoreCase("")) {
            edtAssunto.setError("Por favor, informe o assunto.");

        } else if (edtLocal.getText().toString().trim().equalsIgnoreCase("")) {
            edtLocal.setError("Por favor, informe o local.");

        } else if (edtHora.getText().toString().equalsIgnoreCase("") && !ckbDiaInteiro.isChecked()) {
            edtHora.setError("Por favor, informe a hora.");

        } else if (edtDataInicio.getText().toString().equalsIgnoreCase("")) {
            edtDataInicio.setError("Por favor, informe a hora.");

        } else if (edtDataFim.getText().toString().equalsIgnoreCase("")) {
            edtDataFim.setError("Por favor, informe a hora.");

        } else {
            compromissoBean = new CompromissoBean();
            compromissoBean.setAssunto(edtAssunto.getText().toString());
            compromissoBean.setDescricao(edtDescricao.getText().toString());
            compromissoBean.setLocal(edtLocal.getText().toString());
            compromissoBean.setHora(timeHora);
            compromissoBean.setDiaInteiro(ckbDiaInteiro.isChecked());
            compromissoBean.setIdUser(idUsuario);
            compromissoBean.setDtdInicio(dataInicio.getYear() + "-" + dataInicio.getMonth() + "-" + dataInicio.getDate());
            compromissoBean.setDtdFim(dataFim.getYear() + "-" + dataFim.getMonth() + "-" + dataFim.getDate());
            if(edicaoCompromisso){
                compromissoBean.setIdCompromisso(idCompromisso);
                compromissoBean.setCodfuncao(Constantes.COD_FUNCAO_EDITAR_COMPROMISSO);
            }else{
                compromissoBean.setCodfuncao(Constantes.COD_FUNCAO_INSERIR_COMPROMISSO);
            }
            new CompromissoTask(this, this).execute(compromissoBean);

        }
    }

    private void deleteCompromisso(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Aviso");
        builder.setMessage("Deseja realmente excluir?");
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                compromissoBean = new CompromissoBean();
                compromissoBean.setIdCompromisso(idCompromisso);
                compromissoBean.setCodfuncao(Constantes.COD_FUNCAO_DELETE_COMPROMISSO);
                new CompromissoTask(CadastroCompromissoActivity.this, CadastroCompromissoActivity.this).execute(compromissoBean);
            }
        });

        builder.setNegativeButton("Não", null);
        builder.create();
        builder.show();



    }

    private void hideKeyboard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        hideKeyboard(v);
        switch (v.getId()) {
            case R.id.edtHora:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    onCreateDialogTimePicker().show();
                }
                break;

            case R.id.edtDataInicio:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    onCreateDialogDatePickerInicio(edtDataInicio).show();
                }
                break;

            case R.id.edtDataFim:
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    onCreateDialogDatePickerInicio(edtDataFim).show();
                }
                break;
        }

        return false;
    }

    private Dialog onCreateDialogDatePickerInicio(View view) {
        switch (view.getId()) {
            case R.id.edtDataInicio:
                if (edtDataInicio.getText().toString().trim().equalsIgnoreCase("")) {
                    //Use the current time as the default values for the time picker
                    final Calendar c = Calendar.getInstance();
                    int dia = c.get(Calendar.DAY_OF_MONTH);
                    int mes = c.get(Calendar.MONTH);
                    int ano = c.get(Calendar.YEAR);
                    datePickerDialog = new DatePickerDialog(this, this, ano, mes, dia);
                    //Create and return a new instance of TimePickerDialog
                } else {
                    int dia = calendar.get(Calendar.DAY_OF_MONTH);
                    int mes = calendar.get(Calendar.MONTH);
                    int ano = calendar.get(Calendar.YEAR);
                    datePickerDialog = new DatePickerDialog(this, this, ano, mes, dia);
                }
                return datePickerDialog;

            case R.id.edtDataFim:
                int dia = calendar.get(Calendar.DAY_OF_MONTH);
                int mes = calendar.get(Calendar.MONTH);
                int ano = calendar.get(Calendar.YEAR);
                datePickerDialog = new DatePickerDialog(this, this, ano, mes, dia);
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis());
                return datePickerDialog;
        }
        return null;
    }


    private Dialog onCreateDialogTimePicker() {
        //Use the current time as the default values for the time picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, this, hour, minute, DateFormat.is24HourFormat(this));
        timePickerDialog.setTitle("");
        //Create and return a new instance of TimePickerDialog
        return timePickerDialog;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        String hora;
        String minuto;
        if (hourOfDay < 10) {
            hora = "0" + hourOfDay;
        } else {
            hora = String.valueOf(hourOfDay);
        }

        if (minute < 10) {
            minuto = "0" + minute;
        } else {
            minuto = String.valueOf(minute);
        }

        edtHora.setText(hora + ":" + minuto);
        edtHora.setTextSize(22);
        timeHora = new Time(hourOfDay, minute, 00);
    }


    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        StringBuilder dia = new StringBuilder();
        StringBuilder mes = new StringBuilder();
        if (dayOfMonth < 10) {
            dia.append("0" + dayOfMonth);
        } else {
            dia.append(dayOfMonth);
        }

        if (monthOfYear < 10) {
            mes.append("0" + monthOfYear);
        } else {
            mes.append(monthOfYear);
        }


        if (edtDataInicio.isFocused()) {
            edtDataInicio.setText(dia + "/" + mes + "/" + year);
            edtDataFim.setEnabled(true);
            edtDataFim.setAlpha(1);
            dataInicio.setDate(dayOfMonth);
            dataInicio.setYear(year);
            dataInicio.setMonth(monthOfYear);
            calendar.set(year, monthOfYear, dayOfMonth);
        } else {
            edtDataFim.setText(dayOfMonth + "/" + monthOfYear + "/" + year);
            dataFim.setDate(dayOfMonth);
            dataFim.setYear(year);
            dataFim.setMonth(monthOfYear);
        }

        if (!edtDataFim.getText().toString().equalsIgnoreCase("")) {

            if (dataInicio.after(dataFim)) {
                alertError("A data inicio é posterior que data fim no compromisso.").show();
                edtDataFim.setText("");
            }
        }
    }

    private AlertDialog alertError(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Aviso");
        builder.setMessage(msg);
        builder.setNeutralButton("Ok", null);
        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    @Override
    public void onTaskCompleteCompromissoDelegate(CompromissoBean resultado) {
        if (resultado != null && resultado.getStatusCode() != HttpURLConnection.HTTP_OK) {
            alertError(resultado.getMensagem()).show();
        } else {
            if (resultado.getIdCompromisso() == -1) {
                alertError(resultado.getMensagem()).show();
            } else {
                if(resultado.getRespostaDelete() != Constantes.COD_FUNCAO_DELETE_COMPROMISSO){
                    int diffInDays = (int) ((dataFim.getTime() - dataInicio.getTime()) / (1000 * 60 * 60 * 24));

                    if (diffInDays == 0) {
                        Timer timer = new Timer();
                        listCompromisso.add(compromissoBean);
                        agendaDAO.insert(listCompromisso);
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(dataInicio.getYear(), dataInicio.getMonth(), dataInicio.getDate());
                        calendar.set(Calendar.HOUR_OF_DAY, timeHora.getHours());
                        calendar.set(Calendar.MINUTE, timeHora.getMinutes());
                        calendar.set(Calendar.SECOND, timeHora.getSeconds());
                        Date time = calendar.getTime();
                        timer.schedule(new AlarmeTask(this, timer), time);
                        Toast.makeText(this, "Feito com sucesso", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {

                        Date date = new Date();
                        for (int x = 0; x <= diffInDays; x++) {
                            Timer timer = new Timer();
                            date.setDate(dataInicio.getDate() + x);

                            listCompromisso.add(compromissoBean);
                            agendaDAO.insert(listCompromisso);
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);
                            calendar.set(Calendar.HOUR_OF_DAY, timeHora.getHours());
                            calendar.set(Calendar.MINUTE, timeHora.getMinutes());
                            calendar.set(Calendar.SECOND, timeHora.getSeconds());
                            Date time = calendar.getTime();
                            timer.schedule(new AlarmeTask(this, timer), time);
                        }

                    }
                }
                Toast.makeText(this, "Agendamento feito com sucesso", Toast.LENGTH_SHORT).show();
                finish();

            }
        }
    }
}
