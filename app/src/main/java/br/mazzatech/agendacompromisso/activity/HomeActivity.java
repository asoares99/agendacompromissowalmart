package br.mazzatech.agendacompromisso.activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import br.mazzatech.agendacompromisso.R;
import br.mazzatech.agendacompromisso.dao.AgendaDAO;
import br.mazzatech.agendacompromisso.fragment.CompromissoListFragment;
import br.mazzatech.agendacompromisso.fragment.HomeFragment;
import br.mazzatech.agendacompromisso.fragment.LoginFragment;
import br.mazzatech.agendacompromisso.model.CompromissoBean;
import br.mazzatech.agendacompromisso.model.UserBean;
import br.mazzatech.agendacompromisso.task.PersistirCompromissoDelegate;
import br.mazzatech.agendacompromisso.task.PersistirCompromissoTask;
import br.mazzatech.agendacompromisso.task.UserLoginDelegate;
import br.mazzatech.agendacompromisso.task.UserLoginTask;
import br.mazzatech.agendacompromisso.util.Constantes;

public class HomeActivity extends AppCompatActivity implements UserLoginDelegate, View.OnClickListener, PersistirCompromissoDelegate {
    private Toolbar toolbar;
    private NavigationView navigationView;
    public Menu menu;
    private View view;
    public TextView txtNomeUsuario;
    private DrawerLayout drawerLayout;
    private boolean fragmentHome;
    private static int NAV_ITEM_LOGIN = 0;
    private static int NAV_ITEM_HOME = 1;
    private static int NAV_ITEM_SAIR = 2;
    private ImageView imgPesquisar;
    private AgendaDAO agendaDAO;
    public String emailUser;
    public long IdUsario;
    private List<CompromissoBean> listItens = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        agendaDAO = new AgendaDAO(this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imgPesquisar = (ImageView) toolbar.findViewById(R.id.imgPesquisar);
        imgPesquisar.setOnClickListener(this);
        setSupportActionBar(toolbar);


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        menu = navigationView.getMenu();
        view = navigationView.getHeaderView(0);
        txtNomeUsuario  = (TextView) view.findViewById(R.id.tNome);



        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {


                //Checking if the item is in checked state or not, if not make it in checked state
                menuItem.setChecked(true);

                //Closing drawer on item click
                drawerLayout.closeDrawers();
                onNavDrawerItemSelected(menuItem);
                return true;
            }
        });

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        setNavHome(agendaDAO.verificaLogin());
        agendaDAO.closeDB();
    }

    // Trata o evento do menu lateral
    private void onNavDrawerItemSelected(MenuItem menuItem) {
        Fragment fragment = null;
        Bundle args = new Bundle();

        switch (menuItem.getItemId()) {
            case R.id.nav_item_login:
                fragmentHome = false;
                setNavHome(null);
                break;
            case R.id.nav_item_home:
                fragmentHome = true;
                fragment = new HomeFragment();
                break;

            case R.id.nav_item_sair:
                fragmentHome = true;
                loginSair();
                break;
            case R.id.nav_item_help:
                fragmentHome = false;
                break;
            case R.id.nav_item_config:
                fragmentHome = false;
                break;

        }

        if (fragment != null && menuItem.getOrder() != 4) {
            fragment.setArguments(args);
            FragmentManager frgManager = getSupportFragmentManager();
            frgManager.beginTransaction().replace(R.id.frame, fragment).commit();
        }
    }

    public void setNavHome(UserBean userBean){
        Fragment fragment;
        if (userBean == null || userBean.getEmail() == null){
            txtNomeUsuario.setText("Fazer login");
            menu.getItem(NAV_ITEM_LOGIN).setVisible(true);
            menu.getItem(NAV_ITEM_HOME).setVisible(false);
            menu.getItem(NAV_ITEM_SAIR).setVisible(false);
            menu.getItem(NAV_ITEM_LOGIN).setChecked(true);
            imgPesquisar.setVisibility(View.GONE);
            fragment = new LoginFragment();
            FragmentManager frgManager = getSupportFragmentManager();
            frgManager.beginTransaction().replace(R.id.frame, fragment).commit();
        }else{
            userBean.setManterLogado(true);
            userBean.setCodfuncao(Constantes.COD_FUNCAO_LOGIN);
            new UserLoginTask(this, this).execute(userBean);
        }
    }

    @Override
    public void onTaskCompleteLoginDelegate(UserBean resultado) {
        if (resultado != null && resultado.getStatusCode() != HttpURLConnection.HTTP_OK) {
            alertError(resultado.getMensagem()).show();
            setNavHome(null);
        }else{
            if(resultado.getIdUsuario() == null  || resultado.getIdUsuario() == 0){
                alertError(resultado.getMensagem()).show();
                setNavHome(null);
            }else {
                getEmailUser(resultado.getIdUsuario(), resultado.getEmail() );
                Fragment fragment;
                fragment = new HomeFragment();
                FragmentManager frgManager = getSupportFragmentManager();
                frgManager.beginTransaction().replace(R.id.frame, fragment).commit();
            }
        }
    }

    private void loginSair(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Aviso");
        builder.setMessage("Deseja realmente sair?");
        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                agendaDAO = new AgendaDAO(HomeActivity.this);
                agendaDAO.deletarUser();
                agendaDAO.closeDB();
                setNavHome(null);
            }
        });

        builder.setNegativeButton("Não", null);
        builder.create();
        builder.show();

    }

    public void getEmailUser(long IdUsuario, String email){
        if(!email.equalsIgnoreCase("")){
            this.emailUser = email;
            this.IdUsario = IdUsuario;
        }
        menu.getItem(NAV_ITEM_LOGIN).setVisible(false);
        menu.getItem(NAV_ITEM_HOME).setVisible(true);
        menu.getItem(NAV_ITEM_SAIR).setVisible(true);
        menu.getItem(NAV_ITEM_HOME).setChecked(true);
        imgPesquisar.setVisibility(View.VISIBLE);
        txtNomeUsuario.setText(email);
    }

    public AlertDialog alertError(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Aviso");
        builder.setMessage(msg);
        builder.setNeutralButton("Ok", null);
        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgPesquisar:
                alertDialog().show();
        }
    }



    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private AlertDialog alertDialog(){
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(15,0,15,0);
        layoutParams.setMarginStart(50);
        final EditText edtPesquisa = new EditText(this);
        edtPesquisa.setLayoutParams(layoutParams);
        edtPesquisa.setHint("Pesquisar por assunto");

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pesquisa");
        builder.setView(edtPesquisa);
        builder.setPositiveButton("Pesquisar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(edtPesquisa.getText().toString().trim().equalsIgnoreCase("")){
                    edtPesquisa.setError("Favor, digite aqui para realizar pesquisa.");
                }else{
                    CompromissoBean compromissoBean = new CompromissoBean();
                    compromissoBean.setIdUser(IdUsario);
                    compromissoBean.setAssunto(edtPesquisa.getText().toString().trim());
                    compromissoBean.setTipoConsulta(Constantes.COD_PESQUISA_ASSUNTO);
                    compromissoBean.setCodfuncao(Constantes.COD_FUNCAO_PESQUISA_COMPROMISSO);
                    List<CompromissoBean> listCompromisso = new ArrayList<CompromissoBean>();
                    listCompromisso.add(compromissoBean);
                    new PersistirCompromissoTask(HomeActivity.this, HomeActivity.this).execute(listCompromisso);
                }
            }
        });

        builder.setNegativeButton("Cancelar", null);
        AlertDialog alertDialog = builder.create();
        return alertDialog;
    }

    @Override
    public void onTaskCompleteCompromissoDelegate(List<CompromissoBean> resultado) {
        if(resultado.get(0).getIdCompromisso() != -1){

            listItens = resultado;
            Fragment fragment = new CompromissoListFragment();
            FragmentManager frgManager = getSupportFragmentManager();
            frgManager.beginTransaction().replace(R.id.frame, fragment).commit();
        }else {
            alertError("Não há compromisso(s), cadastrado.").show();
        }

    }

    public List<CompromissoBean> getListCompromisso(){
        return listItens;
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }
    }
}
