package br.mazzatech.agendacompromisso.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.provider.BaseColumns._ID;

import br.mazzatech.agendacompromisso.util.Constantes;

public class DBCore extends SQLiteOpenHelper {

    public DBCore(Context context) {
        super(context, Constantes.DATABASE_NAME, null, Constantes.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase bd) {
        try {
            bd.execSQL(scriptCreateTableUsuarioLogin());
            bd.execSQL(scriptCreateTableCompromisso());
        } catch (SQLiteException e) {
            Log.i("CREATE TABLE", e.getMessage());
        }
    }

    private String scriptCreateTableUsuarioLogin() {
        StringBuffer sbCreateTable = new StringBuffer();
        sbCreateTable.append("CREATE TABLE IF NOT EXISTS ");
        sbCreateTable.append(Constantes.TABLE_USERLOGIN + " ");
        sbCreateTable.append("(" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, ");
        sbCreateTable.append(Constantes.FIELD_USER_EMAIL + " VARCHAR, ");
        sbCreateTable.append(Constantes.FIELD_USER_PASS + " VARCHAR, ");
        sbCreateTable.append(Constantes.FIELD_USER_MANTERLOGIN + " BOOL);");
        return sbCreateTable.toString();
    }

    private String scriptCreateTableCompromisso() {
        StringBuffer sbCreateTable = new StringBuffer();
        sbCreateTable.append("CREATE TABLE IF NOT EXISTS ");
        sbCreateTable.append(Constantes.TABLE_COMPROMISSO + " ");
        sbCreateTable.append("(" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, ");
        sbCreateTable.append(Constantes.FIELD_COMPROMISSO_ASSUNTO + " VARCHAR, ");
        sbCreateTable.append(Constantes.FIELD_COMPROMISSO_DESCRICAO + " VARCHAR, ");
        sbCreateTable.append(Constantes.FIELD_COMPROMISSO_LOCAL + " VARCHAR, ");
        sbCreateTable.append(Constantes.FIELD_COMPROMISSO_HORA + " TIME, ");
        sbCreateTable.append(Constantes.FIELD_COMPROMISSO_DIAINTEIRO + " BOOL, ");
        sbCreateTable.append(Constantes.FIELD_COMPROMISSO_DTDINICIO + " DATE, ");
        sbCreateTable.append(Constantes.FIELD_COMPROMISSO_DTDFIM + " DATE, ");
        sbCreateTable.append(Constantes.FIELD_USER_USER + " INTEGER);");
        return sbCreateTable.toString();
    }

    private String scriptCreateTableIntervalNotif() {
        StringBuffer sbCreateTable = new StringBuffer();
        sbCreateTable.append("CREATE TABLE IF NOT EXISTS ");
        sbCreateTable.append(Constantes.TABLE_NOTIFICACAO + " ");
        sbCreateTable.append("(" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, ");
        sbCreateTable.append("OPLTIPO INT, ");
        sbCreateTable.append("INTERVAL VARCHAR); ");
        return sbCreateTable.toString();
    }

    @Override
    public void onUpgrade(SQLiteDatabase bd, int oldVersion, int newVersion) {
        try {
            bd.execSQL("drop table " + Constantes.TABLE_USERLOGIN + ";");
            bd.execSQL("drop table " + Constantes.TABLE_COMPROMISSO + ";");
        } catch (SQLiteException ex) {
            Log.i(Constantes.DATABASE_NAME, ex.getMessage());
        }
        onCreate(bd);
    }
}
