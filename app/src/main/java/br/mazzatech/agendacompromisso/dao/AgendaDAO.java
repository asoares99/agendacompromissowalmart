package br.mazzatech.agendacompromisso.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import br.mazzatech.agendacompromisso.model.CompromissoBean;
import br.mazzatech.agendacompromisso.model.UserBean;
import br.mazzatech.agendacompromisso.util.Constantes;

import static android.provider.BaseColumns._ID;

/**
 *
 */
public class AgendaDAO {

    private SQLiteDatabase db;

    public AgendaDAO(Context context) {
        DBCore dbCore = new DBCore(context);
        db = dbCore.getWritableDatabase();
    }

    private static final String[] COLS_USER = {
            _ID,
            Constantes.FIELD_USER_EMAIL,
            Constantes.FIELD_USER_PASS,
            Constantes.FIELD_USER_MANTERLOGIN

    };

    private static final String[] COLS_COMPROMISSO = {
            _ID,
            Constantes.FIELD_COMPROMISSO_ASSUNTO,
            Constantes.FIELD_COMPROMISSO_DESCRICAO,
            Constantes.FIELD_COMPROMISSO_LOCAL,
            Constantes.FIELD_COMPROMISSO_HORA,
            Constantes.FIELD_COMPROMISSO_DIAINTEIRO,
            Constantes.FIELD_COMPROMISSO_DTDINICIO,
            Constantes.FIELD_COMPROMISSO_DTDINICIO,
            Constantes.FIELD_USER_USER

    };

    public void insert(List<CompromissoBean> compromissoList) {
        if (compromissoList != null && !compromissoList.isEmpty()) {
            for (CompromissoBean compromisso : compromissoList) {
                long lo = salvar(compromisso);
                System.out.println(lo);
            }
        }
    }

    /**
     * @param compromissoBean
     * @return
     */
    private long salvar(CompromissoBean compromissoBean) {
        if (compromissoBean != null) {
            ContentValues values = new ContentValues();
            values.put(COLS_COMPROMISSO[1], compromissoBean.getAssunto());
            values.put(COLS_COMPROMISSO[2], compromissoBean.getDescricao());
            values.put(COLS_COMPROMISSO[3], compromissoBean.getLocal());
            values.put(COLS_COMPROMISSO[4], String.valueOf(compromissoBean.getHora()));
            values.put(COLS_COMPROMISSO[5], compromissoBean.isDiaInteiro());
            values.put(COLS_COMPROMISSO[6], String.valueOf(compromissoBean.getDtdInicio()));
            values.put(COLS_COMPROMISSO[7], String.valueOf(compromissoBean.getDtdFim()));
            values.put(COLS_COMPROMISSO[8], compromissoBean.getIdUser());

            return db.insert(Constantes.TABLE_COMPROMISSO, null, values);
        }
        return 0;
    }


    public void insertUser(UserBean userBean) {
        if (userBean != null) {

            ContentValues values = new ContentValues();
            values.put(COLS_USER[1], userBean.getEmail());
            values.put(COLS_USER[2], userBean.getPass());
            values.put(COLS_USER[3], userBean.isManterLogado());

            db.insert(Constantes.TABLE_USERLOGIN, null, values);
        }
    }

    public UserBean verificaLogin(){
        UserBean userBean = new UserBean();

        boolean login = false;
        Cursor cursor = db.query(Constantes.TABLE_USERLOGIN, COLS_USER, null, null, null, null, null);
        if(cursor.getCount() > 0){
            cursor.moveToNext();
            userBean.setEmail(cursor.getString(cursor.getColumnIndex(Constantes.FIELD_USER_EMAIL)));
            userBean.setPass(cursor.getString(cursor.getColumnIndex(Constantes.FIELD_USER_PASS)));
        }
        return userBean;
    }

    public void deletarUser(){
        db.delete(Constantes.TABLE_USERLOGIN, null, null);
    }


    public boolean deleteAll() {
        return db.delete(Constantes.TABLE_COMPROMISSO, null, null) > 0;
    }

    public void closeDB() {
        if (db.isOpen()) {
            db.close();
        }
    }

}
