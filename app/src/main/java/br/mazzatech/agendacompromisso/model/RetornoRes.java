package br.mazzatech.agendacompromisso.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RetornoRes implements Serializable {

    private static final long serialVersionUID = -5914526135238362167L;
    @SerializedName("Status")
    @Expose
    private int statusCode;

	private String strRetorno;

    @SerializedName("ErrorMessage")
    @Expose
    private String mensagem;

    @SerializedName("id_usuario")
    @Expose
    private Long idUsuario;


    @SerializedName("codfuncao")
    @Expose
    private int codfuncao;

    @SerializedName("respostaDelete")
    @Expose
    private int respostaDelete;

    public int getCodfuncao() {
        return codfuncao;
    }

    public void setCodfuncao(int codfuncao) {
        this.codfuncao = codfuncao;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStrRetorno() {
        return strRetorno;
    }

    public void setStrRetorno(String strRetorno) {
        this.strRetorno = strRetorno;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getRespostaDelete() {
        return respostaDelete;
    }

    public void setRespostaDelete(int respostaDelete) {
        this.respostaDelete = respostaDelete;
    }
}
