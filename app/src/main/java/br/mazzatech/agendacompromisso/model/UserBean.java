package br.mazzatech.agendacompromisso.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 809549 on 08/06/2016.
 */
public class UserBean extends RetornoRes implements Serializable {
    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("senha")
    @Expose
    private String pass;

    @SerializedName("manterSQLite")
    @Expose
    private boolean manterLogado;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean isManterLogado() {
        return manterLogado;
    }

    public void setManterLogado(boolean manterLogado) {
        this.manterLogado = manterLogado;
    }

}
