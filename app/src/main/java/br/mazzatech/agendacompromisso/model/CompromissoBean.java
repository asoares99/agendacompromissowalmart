package br.mazzatech.agendacompromisso.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.sql.Time;

/**
 * Created by 809549 on 08/06/2016.
 */
public class CompromissoBean extends RetornoRes implements Serializable {
    @SerializedName("COM_CdiCompromisso")
    @Expose
    private int idCompromisso;

    @SerializedName("COM_DssAssunto")
    @Expose
    private String assunto;

    @SerializedName("COM_DssDescricao")
    @Expose
    private String descricao;

    @SerializedName("COM_DssLocal")
    @Expose
    private String local;

    @SerializedName("COM_TimeHora")
    @Expose
    private Time hora;

    @SerializedName("COM_OplDiaTodo")
    @Expose
    private boolean diaInteiro;

    @SerializedName("COM_DtdInicio")
    @Expose
    private String dtdInicio;

    @SerializedName("COM_DtdFim")
    @Expose
    private String dtdFim;

    @SerializedName("COM_CdiUsuario")
    @Expose
    private long idUser;

    @SerializedName("tipoConsulta")
    @Expose
    private int tipoConsulta;

    public int getIdCompromisso() {
        return idCompromisso;
    }

    public void setIdCompromisso(int idCompromisso) {
        this.idCompromisso = idCompromisso;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public Time getHora() {
        return hora;
    }

    public void setHora(Time hora) {
        this.hora = hora;
    }

    public boolean isDiaInteiro() {
        return diaInteiro;
    }

    public void setDiaInteiro(boolean diaInteiro) {
        this.diaInteiro = diaInteiro;
    }

    public String getDtdInicio() {
        return dtdInicio;
    }

    public void setDtdInicio(String dtdInicio) {
        this.dtdInicio = dtdInicio;
    }

    public String getDtdFim() {
        return dtdFim;
    }

    public void setDtdFim(String dtdFim) {
        this.dtdFim = dtdFim;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public int getTipoConsulta() {
        return tipoConsulta;
    }

    public void setTipoConsulta(int tipoConsulta) {
        this.tipoConsulta = tipoConsulta;
    }
}
