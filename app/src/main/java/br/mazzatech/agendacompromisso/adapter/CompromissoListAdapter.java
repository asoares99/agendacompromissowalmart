package br.mazzatech.agendacompromisso.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import br.mazzatech.agendacompromisso.R;
import br.mazzatech.agendacompromisso.model.CompromissoBean;

/**
 * Created by Everton
 */
public class CompromissoListAdapter extends BaseAdapter {

    private List<CompromissoBean> compromissos;
    private LayoutInflater layoutInflater;

    public CompromissoListAdapter(Context context, List<CompromissoBean> compromissos) {
        this.layoutInflater = LayoutInflater.from(context);
        this.compromissos = compromissos;
    }

    @Override
    public int getCount() {
        return compromissos.size();
    }

    @Override
    public Object getItem(int position) {
        return compromissos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        // TODO Auto-generated method stub
        ViewHolder viewHolder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.consulta_item, null);
            viewHolder = new ViewHolder();
            viewHolder.txtAssunto = (TextView) view.findViewById(R.id.txtAssunto);
            viewHolder.txtLocal = (TextView) view.findViewById(R.id.txtLocal);
            viewHolder.txtHora = (TextView) view.findViewById(R.id.txtHora);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.txtAssunto.setText(compromissos.get(position).getAssunto());
        viewHolder.txtLocal.setText(" "+compromissos.get(position).getLocal());

        StringBuilder stringBuilder = new StringBuilder();
        if(compromissos.get(position).getHora().getHours() < 10){
            stringBuilder.append("0");
            stringBuilder.append(compromissos.get(position).getHora().getHours());
        }else {
            stringBuilder.append(compromissos.get(position).getHora().getHours());
        }
        stringBuilder.append(":");
        if(compromissos.get(position).getHora().getMinutes() < 10){
            stringBuilder.append("0");
            stringBuilder.append(compromissos.get(position).getHora().getMinutes());
        }else {
            stringBuilder.append(compromissos.get(position).getHora().getMinutes());
        }
        stringBuilder.append("hs");
        viewHolder.txtHora.setText(stringBuilder.toString());
        return view;
    }

    static class ViewHolder {
        TextView txtAssunto;
        TextView txtLocal;
        TextView txtHora;
    }
}
