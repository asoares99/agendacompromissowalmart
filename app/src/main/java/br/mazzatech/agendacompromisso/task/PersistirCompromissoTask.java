package br.mazzatech.agendacompromisso.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import br.mazzatech.agendacompromisso.model.CompromissoBean;
import br.mazzatech.agendacompromisso.model.RetornoRes;
import br.mazzatech.agendacompromisso.model.UserBean;
import br.mazzatech.agendacompromisso.util.AgendaHelper;
import br.mazzatech.agendacompromisso.util.Constantes;

/**
 * Created by 809549 on 09/06/2016.
 */
public class PersistirCompromissoTask extends AsyncTask<Object, Object, List<CompromissoBean>> {

    private PersistirCompromissoDelegate callback;
    private ProgressDialog progress;
    private Activity activity;
    private AgendaHelper agendaHelper;

    public PersistirCompromissoTask(Activity activity, PersistirCompromissoDelegate callback){
        this.activity = activity;
        this.callback = callback;
        this.agendaHelper = new AgendaHelper();


        progress = new ProgressDialog(activity);
        progress.setMessage("Aguarde, carregando compromissos!");
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected List<CompromissoBean> doInBackground(Object... params) {
        List<CompromissoBean> listCompromisso = new ArrayList<>();

        if(agendaHelper.verificarConexao(activity)){
            try {
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                listCompromisso = (List<CompromissoBean>) params[0];
                CompromissoBean compromisso = listCompromisso.get(0);
                listCompromisso = new ArrayList<>();

                String strLoginJson = gson.toJson(compromisso);
                List<CompromissoBean> listHeader = new ArrayList<>();
                listHeader.add(compromisso);
                RetornoRes resultado = agendaHelper.request(Constantes.URL_CIDADAO_LOGIN, Constantes.REQUEST_METHOD_POST, strLoginJson);

                if(resultado.getStatusCode() == HttpURLConnection.HTTP_OK){
                    try {

                        JSONArray json = (JSONArray) new JSONTokener(resultado.getStrRetorno()).nextValue();

                        if(json != null && json.length() > 0){
                            CompromissoBean compromissoBean;

                            for (int x = 0; x < json.length(); x++){
                                compromissoBean = new CompromissoBean();
                                JSONObject jsonObject = json.getJSONObject(x);

                                Type compromissoType = (Type) new TypeToken<CompromissoBean>() {}.getType();
                                compromissoBean = (CompromissoBean) gson.fromJson(jsonObject.toString(), (Type) compromissoType);
                                compromissoBean.setStatusCode(resultado.getStatusCode());
                                listCompromisso.add(compromissoBean);

                                System.out.println(json.getJSONObject(x));

                            }

                        } else{
                            return listCompromisso;
                        }
                    } catch (ClassCastException e1){
                        if(listCompromisso == null){
                            listCompromisso = new ArrayList<>();
                        }
                        listCompromisso.get(0).setStatusCode(resultado.getStatusCode());
                        listCompromisso.get(0).setMensagem(Constantes.MSG_ERROS);
                    }
                    return listCompromisso;
                } else if(resultado.getStatusCode() == HttpURLConnection.HTTP_CONFLICT){
                    JSONObject json = (JSONObject) new JSONTokener(resultado.getStrRetorno()).nextValue();
                    Type retornoType = (Type) new TypeToken<UserBean>() {}.getType();
                    //compromissoBeanResposta = (CompromissoBean) gson.fromJson(json.toString(), (Type) retornoType);
                   // Log.i("ERRO LOGIN","Erro ao conectar: " + compromissoBeanResposta.getMensagem());
                    return listCompromisso;
                }
            } catch (SocketException e){
                Log.e("SocketException", "" + e);
                listCompromisso.get(0).setMensagem(Constantes.MSG_ERROS);
                return listCompromisso;
            } catch (IOException e) {
                Log.e("IOException", "" + e);
                listCompromisso.get(0).setMensagem(Constantes.MSG_ERROS);
                return listCompromisso;
            } catch (Exception e) {
                Log.e("Exception", "" + e);
                listCompromisso.get(0).setMensagem(Constantes.MSG_ERROS);
                return listCompromisso;
            }
        }

        return listCompromisso;
    }

    @Override
    protected void onPostExecute(List<CompromissoBean> result) {
        super.onPostExecute(result);

        try {
            if (null != progress && progress.isShowing()) {
                progress.dismiss();
            }

            callback.onTaskCompleteCompromissoDelegate(result);
        } catch (final IllegalArgumentException e) {
        } catch (final Exception e) {
        } finally {
            this.progress = null;
        }
    }
}
