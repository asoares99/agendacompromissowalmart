package br.mazzatech.agendacompromisso.task;

import br.mazzatech.agendacompromisso.model.UserBean;

/**
 * Created by 809549 on 09/06/2016.
 */
public interface UserLoginDelegate  {
    public void onTaskCompleteLoginDelegate(UserBean resultado);
}
