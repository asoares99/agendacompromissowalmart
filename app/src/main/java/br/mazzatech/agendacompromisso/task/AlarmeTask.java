package br.mazzatech.agendacompromisso.task;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import br.mazzatech.agendacompromisso.R;
import br.mazzatech.agendacompromisso.activity.HomeActivity;
import br.mazzatech.agendacompromisso.notification.BroadcastManager;

/**
 * Created by 809549 on 09/06/2016.
 */
public class AlarmeTask extends TimerTask {
    private Context context;
    private PendingIntent pendingIntent;
    private Timer timer;

    public AlarmeTask(Context context, Timer timer) {
        this.context = context;
        this.timer = timer;
    }

    public void run() {
        System.out.println("Time's up!");
        try {
            Intent myIntent = new Intent(context, BroadcastManager.class);
            pendingIntent = PendingIntent.getBroadcast(context, 0, myIntent, 0);
            pendingIntent.send();
            timer.cancel();
            pendingIntent.cancel();
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        }

    }

    public void Notification(Context context, String message) {
        // Set Notification Title
        String strtitle = context.getString(R.string.title_notification);
        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(context, HomeActivity.class);
        // Send data to NotificationView Class
        intent.putExtra("title", strtitle);
        intent.putExtra("text", message);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // Create Notification using NotificationCompat.Builder
        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                context)
                // Set Icon
                .setSmallIcon(R.drawable.ic_launcher)
                // Set Ticker Message
                .setTicker(message)
                // Set Title
                .setContentTitle(context.getString(R.string.title_notification))
                // Set Text
                .setContentText(message)
                // Add an Action Button below Notification
                .addAction(R.drawable.ic_launcher, "Compromisso(s)", pIntent)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Dismiss Notification
                .setAutoCancel(true);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(0, builder.build());

    }
}
