package br.mazzatech.agendacompromisso.task;

import java.util.List;

import br.mazzatech.agendacompromisso.model.CompromissoBean;

/**
 * Created by 809549 on 09/06/2016.
 */
public interface PersistirCompromissoDelegate {
    public void onTaskCompleteCompromissoDelegate(List<CompromissoBean> resultado);
}
