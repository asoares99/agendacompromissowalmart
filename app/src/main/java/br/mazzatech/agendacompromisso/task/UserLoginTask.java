package br.mazzatech.agendacompromisso.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import br.mazzatech.agendacompromisso.model.RetornoRes;
import br.mazzatech.agendacompromisso.model.UserBean;
import br.mazzatech.agendacompromisso.util.AgendaHelper;
import br.mazzatech.agendacompromisso.util.Constantes;

/**
 * Created by 809549 on 09/06/2016.
 */
public class UserLoginTask extends AsyncTask<Object, Object, UserBean> {

    private UserLoginDelegate callback;
    private ProgressDialog progress;
    private Activity activity;
    private AgendaHelper agendaHelper;

    public UserLoginTask(Activity activity, UserLoginDelegate callback){
        this.activity = activity;
        this.callback = callback;
        this.agendaHelper = new AgendaHelper();


        progress = new ProgressDialog(activity);
        progress.setMessage("Aguarde, realizando login!");
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected UserBean doInBackground(Object... params) {
        UserBean userBeanResposta = new UserBean();

        if(agendaHelper.verificarConexao(activity)){
            try {
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                UserBean user = (UserBean) params[0];
                String strLoginJson = gson.toJson(user);
                List<UserBean> listHeader = new ArrayList<>();
                listHeader.add(user);
                RetornoRes resultado = agendaHelper.request(Constantes.URL_CIDADAO_LOGIN, Constantes.REQUEST_METHOD_POST, strLoginJson);

                if(resultado.getStatusCode() == HttpURLConnection.HTTP_OK){
                    try {
                        JSONObject json = (JSONObject) new JSONTokener(resultado.getStrRetorno()).nextValue();
                        if(json != null && json.length() > 0){
                            Type agendamentoType = (Type) new TypeToken<UserBean>() {}.getType();
                            userBeanResposta = (UserBean) gson.fromJson(json.toString(), (Type) agendamentoType);
                            userBeanResposta.setStatusCode(resultado.getStatusCode());
                        } else{
                            return userBeanResposta;
                        }
                    } catch (ClassCastException e1){
                        if(userBeanResposta == null){
                            userBeanResposta = new UserBean();
                        }
                        userBeanResposta.setStatusCode(resultado.getStatusCode());
                        userBeanResposta.setMensagem(Constantes.MSG_ERROS);
                    }
                    return userBeanResposta;
                } else if(resultado.getStatusCode() == HttpURLConnection.HTTP_CONFLICT){
                    JSONObject json = (JSONObject) new JSONTokener(resultado.getStrRetorno()).nextValue();
                    Type retornoType = (Type) new TypeToken<UserBean>() {}.getType();
                    userBeanResposta = (UserBean) gson.fromJson(json.toString(), (Type) retornoType);
                    Log.i("ERRO LOGIN","Erro ao fazer o login: " + userBeanResposta.getMensagem());
                    return userBeanResposta;
                } else {
                    if(userBeanResposta == null){
                        userBeanResposta = new UserBean();
                    }
                    userBeanResposta.setStatusCode(resultado.getStatusCode());
                    userBeanResposta.setStrRetorno(resultado.getStrRetorno());
                    userBeanResposta.setMensagem(resultado.getMensagem());
                    return userBeanResposta;
                }
            } catch (SocketException e){
                Log.e("SocketException", "" + e);
                userBeanResposta.setMensagem(Constantes.MSG_ERROS);
                return userBeanResposta;
            } catch (IOException e) {
                Log.e("IOException", "" + e);
                userBeanResposta.setMensagem(Constantes.MSG_ERROS);
                return userBeanResposta;
            } catch (Exception e) {
                Log.e("Exception", "" + e);
                userBeanResposta.setMensagem(Constantes.MSG_ERROS);
                return userBeanResposta;
            }
        }

        return userBeanResposta;
    }

    @Override
    protected void onPostExecute(UserBean result) {
        super.onPostExecute(result);

        try {
            if (null != progress && progress.isShowing()) {
                progress.dismiss();
            }

            callback.onTaskCompleteLoginDelegate(result);
        } catch (final IllegalArgumentException e) {
        } catch (final Exception e) {
        } finally {
            this.progress = null;
        }
    }
}
