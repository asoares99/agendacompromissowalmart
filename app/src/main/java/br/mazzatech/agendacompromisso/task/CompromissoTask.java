package br.mazzatech.agendacompromisso.task;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;

import br.mazzatech.agendacompromisso.model.CompromissoBean;
import br.mazzatech.agendacompromisso.model.RetornoRes;
import br.mazzatech.agendacompromisso.model.UserBean;
import br.mazzatech.agendacompromisso.util.AgendaHelper;
import br.mazzatech.agendacompromisso.util.Constantes;

/**
 * Created by 809549 on 09/06/2016.
 */
public class CompromissoTask extends AsyncTask<Object, Object, CompromissoBean> {

    private CompromissoDelegate callback;
    private ProgressDialog progress;
    private Activity activity;
    private AgendaHelper agendaHelper;

    public CompromissoTask(Activity activity, CompromissoDelegate callback){
        this.activity = activity;
        this.callback = callback;
        this.agendaHelper = new AgendaHelper();


        progress = new ProgressDialog(activity);
        progress.setMessage("Aguarde, salvando compromisso!");
        progress.setIndeterminate(true);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected CompromissoBean doInBackground(Object... params) {
        CompromissoBean compromissoBeanResposta = new CompromissoBean();

        if(agendaHelper.verificarConexao(activity)){
            try {
                Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
                CompromissoBean compromisso = (CompromissoBean) params[0];
                String strLoginJson = gson.toJson(compromisso);
                List<CompromissoBean> listHeader = new ArrayList<>();
                listHeader.add(compromisso);
                RetornoRes resultado = agendaHelper.request(Constantes.URL_CIDADAO_LOGIN, Constantes.REQUEST_METHOD_POST, strLoginJson);

                if(resultado.getStatusCode() == HttpURLConnection.HTTP_OK){
                    try {
                        JSONObject json = (JSONObject) new JSONTokener(resultado.getStrRetorno()).nextValue();

                        if(json != null && json.length() > 0){
                            Type compromissoType = (Type) new TypeToken<CompromissoBean>() {}.getType();
                            compromissoBeanResposta = (CompromissoBean) gson.fromJson(json.toString(), (Type) compromissoType);
                            compromissoBeanResposta.setStatusCode(resultado.getStatusCode());
                        } else{
                            return compromissoBeanResposta;
                        }
                    } catch (ClassCastException e1){
                        if(compromissoBeanResposta == null){
                            compromissoBeanResposta = new CompromissoBean();
                        }
                        compromissoBeanResposta.setStatusCode(resultado.getStatusCode());
                        compromissoBeanResposta.setMensagem(Constantes.MSG_ERROS);
                    }
                    return compromissoBeanResposta;
                } else if(resultado.getStatusCode() == HttpURLConnection.HTTP_CONFLICT){
                    JSONObject json = (JSONObject) new JSONTokener(resultado.getStrRetorno()).nextValue();
                    Type retornoType = (Type) new TypeToken<UserBean>() {}.getType();
                    compromissoBeanResposta = (CompromissoBean) gson.fromJson(json.toString(), (Type) retornoType);
                    Log.i("ERRO LOGIN","Erro ao conectar: " + compromissoBeanResposta.getMensagem());
                    return compromissoBeanResposta;
                } else {
                    if(compromissoBeanResposta == null){
                        compromissoBeanResposta = new CompromissoBean();
                    }
                    compromissoBeanResposta.setStatusCode(resultado.getStatusCode());
                    compromissoBeanResposta.setStrRetorno(resultado.getStrRetorno());
                    compromissoBeanResposta.setMensagem(resultado.getMensagem());
                    return compromissoBeanResposta;
                }
            } catch (SocketException e){
                Log.e("SocketException", "" + e);
                compromissoBeanResposta.setMensagem(Constantes.MSG_ERROS);
                return compromissoBeanResposta;
            } catch (IOException e) {
                Log.e("IOException", "" + e);
                compromissoBeanResposta.setMensagem(Constantes.MSG_ERROS);
                return compromissoBeanResposta;
            } catch (Exception e) {
                Log.e("Exception", "" + e);
                compromissoBeanResposta.setMensagem(Constantes.MSG_ERROS);
                return compromissoBeanResposta;
            }
        }

        return compromissoBeanResposta;
    }

    @Override
    protected void onPostExecute(CompromissoBean result) {
        super.onPostExecute(result);

        try {
            if (null != progress && progress.isShowing()) {
                progress.dismiss();
            }

            callback.onTaskCompleteCompromissoDelegate(result);
        } catch (final IllegalArgumentException e) {
        } catch (final Exception e) {
        } finally {
            this.progress = null;
        }
    }
}
