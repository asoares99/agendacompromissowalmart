package br.mazzatech.agendacompromisso.util;

import android.content.Context;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import br.mazzatech.agendacompromisso.model.RetornoRes;
import br.mazzatech.agendacompromisso.model.UserBean;

/**
 * Created by 809549 on 09/06/2016.
 */
public class AgendaHelper {


    public boolean verificarConexao(Context context){
        ConnectionDetector cd = new ConnectionDetector(context);
        return cd.isConnectingToInternet();
 //       return true;
    }

    public RetornoRes request(String urlString, String method, String json) throws SocketTimeoutException, IOException, Exception {
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        RetornoRes retorno = new RetornoRes();

        try {
            URL url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(Constantes.READ_TIMEOUT);
            connection.setConnectTimeout(Constantes.CONNECT_TIMEOUT);
            connection.setRequestMethod(method);
            connection.setRequestProperty(Constantes.WS_ACCEPT, Constantes.WS_APPLICATION_URLENCODES);
            connection.setRequestProperty(Constantes.WS_CONTENT_TYPE, Constantes.WS_APPLICATION_URLENCODES);
            connection.setDoInput(true);


            if(method.equalsIgnoreCase(Constantes.REQUEST_METHOD_POST) || method.equalsIgnoreCase(Constantes.REQUEST_METHOD_PUT)){
                if(json != null && !json.isEmpty()){
                    connection.setRequestProperty(Constantes.REQUEST_PROPERTY_CONTENT_LENGHT, String.valueOf(json.getBytes().length));
                    OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                    writer.write(json);
                    writer.flush();
                    writer.close();
                }
            }

            connection.connect();

            if(connection.getResponseCode() == HttpURLConnection.HTTP_OK){
                inputStream = connection.getInputStream();
                retorno.setStatusCode(connection.getResponseCode());
                retorno.setStrRetorno(convertStreamToStringRequest(inputStream));
            } else if(connection.getResponseCode() == HttpURLConnection.HTTP_CONFLICT){
                inputStream = connection.getErrorStream();
                retorno.setStatusCode(connection.getResponseCode());
                retorno.setStrRetorno(convertStreamToStringRequest(inputStream));
            } else if(connection.getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST){
                inputStream = connection.getErrorStream();
                retorno.setStatusCode(connection.getResponseCode());
                retorno.setStrRetorno(convertStreamToStringRequest(inputStream));
                retorno.setMensagem(getErrorMessage(retorno.getStrRetorno()));
            } else if(connection.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED){
                inputStream = connection.getErrorStream();
                retorno.setStatusCode(connection.getResponseCode());
                retorno.setStrRetorno(convertStreamToStringRequest(inputStream));
                retorno.setMensagem(getErrorMessage(retorno.getStrRetorno()));
            }else{
                retorno.setStatusCode(connection.getResponseCode());
                retorno.setStrRetorno("Ocorreram erros ao consultar o o servico da Cetesb");
            }
            return retorno;
        } finally {
            inputStream.close();
            connection.disconnect();
        }
    }

    private String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public String convertStreamToStringRequest(InputStream is) throws IOException {
        if (is != null) {
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }

    private String getErrorMessage(String valor){
        if(valor != null && !"".equals(valor.trim())){
            JsonParser parser = new JsonParser();
            JsonObject obj = parser.parse(valor).getAsJsonObject();
            try{
                return obj.get("Message").getAsString();
            } catch (NullPointerException ex){
                return "";
            }
        }
        return "";
    }
}
