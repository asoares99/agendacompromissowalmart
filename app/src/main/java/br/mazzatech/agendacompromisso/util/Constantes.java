package br.mazzatech.agendacompromisso.util;

/**
 * Created by 809549 on 08/06/2016.
 */
public class Constantes {
    /**
     * WEB SERVICE
     */
    public static final String URL_CIDADAO_LOGIN = "http://localhost:8080/ws/index.php";
    public static final int COD_FUNCAO_LOGIN = 001;
    public static final int COD_FUNCAO_INSERIR_COMPROMISSO = 002;
    public static final int COD_FUNCAO_EDITAR_COMPROMISSO = 003;
    public static final int COD_FUNCAO_DELETE_COMPROMISSO = 004;
    public static final int COD_FUNCAO_PERSISTIR_COMPROMISSO = 005;
    public static final int COD_FUNCAO_PESQUISA_COMPROMISSO = 006;

    public static final int COD_PESQUISA_ASSUNTO = 1;


    /** database **/
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "AGENDA";
    public static final String TABLE_USERLOGIN = "USERLOGIN";
    public static final String TABLE_COMPROMISSO = "COMPROMISSO";
    public static final String TABLE_NOTIFICACAO = "NOTIFICACAO";

    public static final String FIELD_USER_EMAIL = "EMAIL";
    public static final String FIELD_USER_PASS = "PASS";
    public static final String FIELD_USER_MANTERLOGIN = "LOGIN";


    public static final String FIELD_COMPROMISSO_ASSUNTO = "ASSUNTO";
    public static final String FIELD_COMPROMISSO_DESCRICAO = "DESCRICAO";
    public static final String FIELD_COMPROMISSO_LOCAL = "LOCAL";
    public static final String FIELD_COMPROMISSO_HORA = "HORA";
    public static final String FIELD_COMPROMISSO_DIAINTEIRO = "DIAINTEIRO";
    public static final String FIELD_COMPROMISSO_DTDINICIO = "DTDINICIO";
    public static final String FIELD_COMPROMISSO_DTDFIM = "DTDFIM";
    public static final String FIELD_USER_USER = "USER";

    public static final String WS_CONTENT_TYPE = "Content-Type";
    public static final String WS_APPLICATION_JSON = "application/json";
    public static final String WS_APPLICATION_URLENCODES = "application/x-www-form-urlencoded";
    public static final String WS_APPLICATION_HTML = "text/html";


    public static final String WS_ACCEPT = "Accept";
    public static final String WS_CONTENT_TYPE_TEXT = "text/json";
    public static final String REQUEST_METHOD_GET = "GET";
    public static final String REQUEST_METHOD_POST = "POST";
    public static final String REQUEST_METHOD_PUT = "PUT";
    public static final String REQUEST_PROPERTY_AUTHORIZATION = "Authorization";
    public static final String REQUEST_PROPERTY_CONTENT_LENGHT = "Content-Length";
    public static final int READ_TIMEOUT = 10000;
    public static final int CONNECT_TIMEOUT = 15000;



    public static final String MSG_ERROS = "Ocorreram erros ao acessar as informações, verifique sua conexão e tente novamente";


}
