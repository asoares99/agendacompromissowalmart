CREATE DATABASE  IF NOT EXISTS `agenda` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `agenda`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: agenda
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `compromisso`
--

DROP TABLE IF EXISTS `compromisso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `compromisso` (
  `COM_CdiCompromisso` int(11) NOT NULL AUTO_INCREMENT,
  `COM_DssAssunto` varchar(45) NOT NULL,
  `COM_DssDescricao` varchar(100) DEFAULT NULL,
  `COM_DssLocal` varchar(45) NOT NULL,
  `COM_TimeHora` varchar(45) DEFAULT NULL,
  `COM_OplDiaTodo` int(11) DEFAULT '0',
  `COM_DtdInicio` date NOT NULL,
  `COM_DtdFim` date NOT NULL,
  `COM_CdiUsuario` int(11) NOT NULL,
  PRIMARY KEY (`COM_CdiCompromisso`),
  KEY `COM_User_idx` (`COM_CdiUsuario`),
  CONSTRAINT `COM_User` FOREIGN KEY (`COM_CdiUsuario`) REFERENCES `ususario` (`USU_CdiUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `compromisso`
--

LOCK TABLES `compromisso` WRITE;
/*!40000 ALTER TABLE `compromisso` DISABLE KEYS */;
INSERT INTO `compromisso` VALUES (1,'Família ','ações ','Casa','03:00:00 PM',0,'2016-05-10','2016-05-10',1);
/*!40000 ALTER TABLE `compromisso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ususario`
--

DROP TABLE IF EXISTS `ususario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ususario` (
  `USU_CdiUsuario` int(11) NOT NULL,
  `USU_CosEmail` varchar(45) NOT NULL,
  `USU_CosSenha` varchar(45) NOT NULL,
  `USU_OplAtivo` int(11) NOT NULL,
  PRIMARY KEY (`USU_CdiUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ususario`
--

LOCK TABLES `ususario` WRITE;
/*!40000 ALTER TABLE `ususario` DISABLE KEYS */;
INSERT INTO `ususario` VALUES (1,'alex.soares99@gmail.com','aa1bf4646de67fd9086cf6c79007026c',1);
/*!40000 ALTER TABLE `ususario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-10 15:06:49
