<?php
//Chama Arquivo de Conexão com o Banco de Dados
include_once "config/conexao.php";


//Codifica pagina para Utf-8
header('Content-Type: application/json; charset=utf-8');

$data = json_decode(file_get_contents('php://input'), true);

//print_r($headers);
// ------------------------------------------------------------------------------------------------
// Variaveis Globais
$funcao = $data["codfuncao"];

// ------------------------------------------------------------------------------------------------
// Funções - NÃO MODIFICAR OS CÓDIGOS DAS FUNÇÕES
// ------------------------------------------------------------------------------------------------
// 001 - Atenticação do Usuário
// 002 - Insert Compromisso
// 005 - Persistir Compromissos

if ($funcao == "001") {
	$email  = $data["email"];
	$senha  = $data["senha"];

	//Busca no banco de dados os dados
	$sql = "SELECT * FROM ususario WHERE USU_CosEmail = '".$email."' AND USU_CosSenha = MD5('".$senha."') AND USU_OplAtivo = 1 LIMIT 1";
	$retorno 	= mysql_query($sql);
	$resultado 	= mysql_num_rows($retorno);
	
	//Faz tratativa para o APP, se o resultado for igual a 1 ou 0
	if ($resultado == 0 AND $resultado == "") { 
		echo '{"Status":"-1", "ErrorMessage":"Não foi possível logar, tente novamente.", "id_usuario":"0"}';
	}elseif ($resultado == 1){ 
		while ($registro = mysql_fetch_array($retorno)){
			$codUsuario = $registro['USU_CdiUsuario'];
			$nomeUsuario = $registro['USU_CosEmail'];
		};

		echo '{"Status":"200", "ErrorMessage":"", "id_usuario":"'.$codUsuario.'", "email":"'.$nomeUsuario.'"}';
	};
};


//Insert Compromisso
if ($funcao == "002") {

	$assunto  = $data["COM_DssAssunto"];
	$descricao = $data["COM_DssDescricao"];
	$local = $data["COM_DssLocal"];
	$hora = $data["COM_TimeHora"];
	$oplDiaInteiro = $data["COM_OplDiaTodo"];
	$dtdInicio = $data["COM_DtdInicio"];
	$dtdFim = $data["COM_DtdFim"];
	$idUsuario  = $data["COM_CdiUsuario"];


	$query = "INSERT INTO compromisso
					        (COM_DssAssunto,
					         COM_DssDescricao,
					         COM_DssLocal,
					         COM_TimeHora,
					         COM_OplDiaTodo,
					         COM_DtdInicio,
					         COM_DtdFim,
					         COM_CdiUsuario)
					VALUES ('".$assunto."',
					        '".$descricao."',
					        '".$local."',
					        '".$hora."',
					        '".$oplDiaInteiro."',
					        '".$dtdInicio."',
					        '".$dtdFim."',
					        '".$idUsuario."')";

    $retorno 	= mysql_query($query);
	//
	if ($retorno != 0){
		echo '{"Status":"200", "ErrorMessage":"", "COM_CdiCompromisso":"'.$retorno.'"}';
	}else{
		echo '{"Status":"-1", "ErrorMessage":"Não foi possível salvar, tente novamente.", "COM_CdiCompromisso":"-1"}';		
	};

};

//Update Compromisso
if ($funcao == "003") {
	$idCompromisso = $data["COM_CdiCompromisso"];
	$assunto  = $data["COM_DssAssunto"];
	$descricao = $data["COM_DssDescricao"];
	$local = $data["COM_DssLocal"];
	$hora = $data["COM_TimeHora"];
	$oplDiaInteiro = $data["COM_OplDiaTodo"];
	$dtdInicio = $data["COM_DtdInicio"];
	$dtdFim = $data["COM_DtdFim"];


	$query = "UPDATE compromisso SET COM_DssAssunto = '".$assunto."', COM_DssDescricao = '".$descricao."', COM_DssLocal = '".$local."', 
									 COM_TimeHora = '".$hora."', COM_OplDiaTodo = '".$oplDiaInteiro."', COM_DtdInicio = '".$dtdInicio."', 
									 COM_DtdFim = '".$dtdFim."' WHERE COM_CdiCompromisso ='".$idCompromisso."'";

    $retorno 	= mysql_query($query);
	//
	if ($retorno != 0){
		echo '{"Status":"200", "ErrorMessage":"", "COM_CdiCompromisso":"'.$retorno.'"}';
	}else{
		echo '{"Status":"-1", "ErrorMessage":"Não foi possível alterar seu compromisso, tente novamente.", "COM_CdiCompromisso":"-1"}';		
	};

};


//Delete Compromisso
if ($funcao == "004") {
	$idCompromisso = $data["COM_CdiCompromisso"];

	$query = "DELETE FROM compromisso WHERE COM_CdiCompromisso ='".$idCompromisso."'";

    $retorno = mysql_query($query);
	//
	if ($retorno != 0){
		echo '{"Status":"200", "ErrorMessage":"", "COM_CdiCompromisso":"'.$retorno.'","respostaDelete":"004"}';
	}else{
		echo '{"Status":"-1", "ErrorMessage":"Não foi possível deletar seu compromisso, tente novamente.", "COM_CdiCompromisso":"-1"}';		
	};

};




if ($funcao == "005") {

	$idUsuario  = $data["COM_CdiUsuario"];	

	// //Busca no banco de dados os dados
	$sql = "SELECT DISTINCT * FROM compromisso WHERE COM_CdiUsuario = ".$idUsuario;
	$retorno 	= mysql_query($sql);
	$resultado 	= mysql_num_rows($retorno);

	// //Faz tratativa para o APP, se o resultado for igual a 1 ou 0
	if ($resultado == 0 AND $resultado == "") { 
		echo '[{"Status":"-1", "ErrorMessage":"Não foi possível exibir os compromissos, tente novamente.", "COM_CdiCompromisso":"-1"}]';
	}else{ 
		while ($registro = mysql_fetch_array($retorno)){
				$objeto[] = $registro;
			};
			//Codifica o array para json
			$json = json_encode($objeto);
			
			//Verifica se houve algum erro no json
			if (json_last_error() == 0){
				echo $json;
		};
		
	};
};

//Pesquisa Compromisso
if ($funcao == "006") {

	$idUsuario  = $data["COM_CdiUsuario"];	
	$tipoConsulta = $data["tipoConsulta"];

	if($tipoConsulta == 1){
		$assunto = $data["COM_DssAssunto"];
		$sql = "SELECT DISTINCT * FROM compromisso WHERE COM_DssAssunto like '%".$assunto."%' AND COM_CdiUsuario = ".$idUsuario;

	}elseif ($tipoConsulta == 2) {

	}elseif ($tipoConsulta == 3) {

	}elseif ($tipoConsulta == 4) {
	}

	// //Busca no banco de dados os dados
	
	$retorno 	= mysql_query($sql);
	$resultado 	= mysql_num_rows($retorno);

	// //Faz tratativa para o APP, se o resultado for igual a 1 ou 0
	if ($resultado == 0 AND $resultado == "") { 
		echo '[{"Status":"-1", "ErrorMessage":"Não foi possível exibir os compromissos, tente novamente.", "COM_CdiCompromisso":"-1"}]';
	}else{ 
		while ($registro = mysql_fetch_array($retorno)){
				$objeto[] = $registro;
			};
			//Codifica o array para json
			$json = json_encode($objeto);
			
			//Verifica se houve algum erro no json
			if (json_last_error() == 0){
				echo $json;
		};
		
	};
};



?>